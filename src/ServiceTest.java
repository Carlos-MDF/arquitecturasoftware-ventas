import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ServiceTest {
private ProductSaleProcess service = new Service();
	
	@Rule
	public ExpectedException expectedException = ExpectedException.none();
	
	@Test
	public void amountTestWith1() throws Exception {
		service.setPrice(1);
		assertThat(service.estimateRate(), is(25));
	}
	
	@Test
	public void amountTestWith20() throws Exception {
		service.setPrice(20);
		assertThat(service.estimateRate(), is(500));
	}
	
	@Test
	public void checkName() {
		service.setName("Reparacion de Celular");
		assertThat("Reparacion de Celular", is(service.getName()));;
	}
	
	@Test
	public void amountTestWithNegativeValue() throws Exception {
		expectedException.expect(Exception.class);
		service.setPrice(-1);
	}
}
