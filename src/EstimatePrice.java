
public interface EstimatePrice {
	public int calculatePrice(int price);
}
