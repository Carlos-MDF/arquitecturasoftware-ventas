
public class Article extends ProductSaleProcess {
	
	public Article() {
		setEstimatePrice(new ArticlePrice());
	}
	

	public boolean isAvailable(int amount) {
		if (amount > 20) {
			return false;
		}
		return true;
	}
}
