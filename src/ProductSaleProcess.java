
public abstract class ProductSaleProcess implements Product {

	private EstimatePrice estimation;
	
	private String name;
	
	private int price;
	
	@Override
	public abstract boolean isAvailable(int amount);
	@Override
	public int estimateRate() {
		return estimation.calculatePrice(price);
	}
	
	public void setEstimatePrice(EstimatePrice newEstimation) {
		estimation = newEstimation;
	}
	
	public void setName(final String newName) {
		name = newName;
	}
	
	public String getName() {
		return name;
	}
	
	public int getPrice() {
		return price;
	}
	
	public void setPrice(int newPrice) throws Exception {
		if (newPrice < 0) {
			throw new Exception("The price can not be negative");
		}
		price = newPrice;
	}

}
