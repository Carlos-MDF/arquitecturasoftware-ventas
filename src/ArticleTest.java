import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ArticleTest {
	private ProductSaleProcess article = new Article();

	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Test
	public void amountTestWith1() throws Exception {
		article.setPrice(1);
		assertThat(article.estimateRate(), is(44));
	}

	@Test
	public void amountTestWith20() throws Exception {
		article.setPrice(20);
		assertThat(article.estimateRate(), is(880));
	}

	@Test
	public void checkName() {
		article.setName("Mentisan");
		assertThat("Mentisan", is(article.getName()));
		;
	}

	@Test
	public void amountTestWithNegativeValue() throws Exception {
		expectedException.expect(Exception.class);
		article.setPrice(-1);
	}
}
