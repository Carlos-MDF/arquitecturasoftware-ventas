
public interface Product {
	
	boolean isAvailable(int amount);
	
	int estimateRate();
}
