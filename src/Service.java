
public class Service extends ProductSaleProcess {

	@Override
	public boolean isAvailable(int amount) {
		if(amount > 2) {
			return false;
		}
		return true;
	}
	
	public Service() {
		setEstimatePrice(new ServicePrice());
	}

}
